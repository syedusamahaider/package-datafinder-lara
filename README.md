# DataFinder - Laravel Package

## Description 

# This is package to implement a search and filters on any system using data-tables on frontend along with Filters and Eloquent Query Builder of Laravel. Best part of this package is to search in multiple tables and it's any row in a single module or request (ajax), using or without using JOINS. Get data from any table using JOINS. Fast Search. It unique thing is, just add search or filter column in single file for any module of whole system and its implemented.

## Important Guide

# Please follow the below installation process as it is written down to integrate this plugin smoothly.

## Installation

# composer require suhk/datafinder-laravel

## Important CDNs

#  Adding Bootstrap | Jquery | Datatable CDN to Header tag is Important

## Adding Provider

# On installing package, Provider will auto-loaded to the project but if it is missing, please add following to ""Providers" array in "config/app.php" file 

#### SUHK\DataFinder\App\Providers\MainServiceProvider::class

## Documentation is in process, it will be uploaded soon. Please your patience is required. Thanks